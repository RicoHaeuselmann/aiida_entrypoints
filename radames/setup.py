from setuptools import setup

setup(
    name='radames',
    version='1.0a1',
    description='Minimal example for a plugin for amneris',
    author='Rico Häuselmann',
    packages=['radames'],
    classifiers=[
        'Development Status :: 3 - Alpha',
    ],
    entry_points={
        'amneris.calc': ['radames = radames.calc']
    }
)
