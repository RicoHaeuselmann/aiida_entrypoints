import pkg_resources as pr

_toplevel_package = __package__.split('.')[0]
_plugin_kw_map = {
    'Parser': 'parser',
    'Calculation': 'calc',
    'Workflow': 'workflow',
}

_builtins = {
    'parser': ['builtin = amneris.builtins.parsers'],
    'calc': ['builtin = amneris.builtins.calc'],
    'workflow': ['builtin = amneris.builtins.workflows']
}


def get_entry_points():
    return {_toplevel_package+'.'+v: _builtins[v]
            for v in _plugin_kw_map.itervalues()}


def list_plugins(plugin_type=None):
    '''returns a python dict of plugins by type, name, info'''
    all_entry_points = [e for e
                        in pr.get_entry_map(_toplevel_package).iterkeys()
                        if e.startswith(_toplevel_package)]
    if not plugin_type:
        plist = {}
        for e in all_entry_points:
            plist[e] = list_plugins(e)
        return plist
    elif plugin_type in all_entry_points:
        ptlist = {}
        for plugin in pr.iter_entry_points(group=plugin_type):
            pitem = {}
            target = plugin.resolve()
            if hasattr(target, '__module__'):
                package = target.__module__.split('.')[0]
            else:
                package = target.__name__.split('.')[0]
            dist = pr.get_distribution(package)
            pitem['package'] = {
                'name': package,
                'version': dist.version
            }
            pitem['module'] = plugin.resolve().__name__
            ptlist[plugin.name] = pitem
        return ptlist
    else:
        print('no entrypoint named ' + plugin_type)
        return None


def print_plugin_list(plugin_type=None):
    plugin_list = list_plugins(plugin_type)
    type_str = '{type} plugin:\n{plugins}'
    plugin_str = ('\t{name}\n'
                  '\t\tpackage: {package}\n'
                  '\t\tversion: {version}\n'
                  '\t\tmodule: {module}')
    list_str = ''
    for pt, pl in plugin_list.iteritems():
        pl_str_lst = []
        for name, p in pl.iteritems():
            pl_str = plugin_str.format(
                name=name,
                package=p['package']['name'],
                version=p['package']['version'],
                module=p['module']
            )
            pl_str_lst.append(pl_str)
        plt_str = '\n'.join(pl_str_lst)
        list_str += type_str.format(type=pt, plugins=plt_str)
        list_str += '\n'
    print(list_str)


def load_plugin(spec):
    mpath = spec.split('.')
    ep = mpath.pop(0)
    name = '.'.join(mpath)
    ep = _toplevel_package + '.' + ep
    setattr(ep, name, pr.load_entry_point(_toplevel_package, ep, name))


def get_plugin(name, ptype='calc'):
    n = name.split('.')
    distn = n[0]
    plugin = None
    try:
        plugin = pr.load_entry_point(distn, 'amneris.'+ptype, name)
    except ImportError as e:
        distn = 'amneris'
        try:
            plugin = pr.load_entry_point(distn, 'amneris.'+ptype, name)
        except ImportError as f:
            classn = n.pop(-1)
            distn = n[0]
            name = '.'.join(n)
            try:
                plugin_m = pr.load_entry_point(distn, 'amneris.'+ptype, name)
                plugin = getattr(plugin_m, classn)
            except ImportError as g:
                distn = 'amneris'
                try:
                    plugin_m = pr.load_entry_point(
                        distn, 'amneris.'+ptype, name)
                    plugin = getattr(plugin_m, classn)
                except ImportError as h:
                    print(e)
                    print(f)
                    print(g)
                    raise h
    return plugin


def get_calc(name):
    return get_plugin(name, ptype='calc')


def get_parser(name):
    return get_plugin(name, ptype='parser')


def get_workflow(name):
    return get_plugin(name, ptype='workflow')
